package com.app.mdm.utils

import android.util.Log
import android.widget.Toast
import java.lang.Exception
import java.lang.StringBuilder
import java.net.NetworkInterface
import java.util.*

object GlobalFunction {


    fun showToast(message :String){
        Toast.makeText(MyApplication.getAppInstance(),message , Toast.LENGTH_SHORT).show()
    }


    fun getMacAddress(): String {
        try {
            val all: List<NetworkInterface> =
                Collections.list(NetworkInterface.getNetworkInterfaces())
            for (networkInterface in all) {
                if (!networkInterface.name.equals("wlan0")) continue
                val macBytes: ByteArray = networkInterface.getHardwareAddress() ?: return ""
                val res1 = StringBuilder()
                Log.e("get mac", "getMacAddr: $res1")
                for (b in macBytes) {
                    // res1.append(Integer.toHexString(b & 0xFF) + ":");
                    res1.append(String.format("%02X:", b))
                }
                if (res1.isNotEmpty()) {
                    res1.deleteCharAt(res1.length - 1)
                }
                return res1.toString().replace(":", "-")
            }
        } catch (ex: Exception) {
            Log.e("TAG", "getMacAddr: ", ex)
        }
        return ""
    }
}