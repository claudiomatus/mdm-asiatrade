package com.app.mdm.utils

object Constants {


    const val MAC = "mac"
    const val PACKAGE ="package"
    const val NAME ="name"
    const val NO_INTERNET_CONNECTION = "no internet connection"
    const val BROADCAST_KEY ="broadcast_key"
    const val BLOCKED = "blocked"
    const val WHITE_LIST_APP = "white_list_app"
    const val INSTALLED_APPS = "installedApps"
    const val REFRESH_KEY ="refresh_key"
    const val BLOCK = "block"
    const val UNLOCK = "unlock"
}