package com.app.mdm.utils

import android.content.Context

object SharedPrefHelper {

    private const val MAC_ID = "mac_id"
    private const val WHITE_LIST = "white_list"
    private const val BLACK_LIST = "black_list"
    private const val STATUS = "status"


    fun setMacId(context: Context, id: String) {
        SharedPrefUtil.writeStringToSharedPreference(context, MAC_ID, id)
    }

    fun getMacId(context: Context): String? {
        return SharedPrefUtil.readStringFromSharedPreference(context, MAC_ID)
    }

    fun setWhiteList(context: Context, firstTime: String) {
        SharedPrefUtil.writeStringToSharedPreference(context, WHITE_LIST, firstTime)
    }

    fun getWhiteList(context: Context): String? {
        return SharedPrefUtil.readStringFromSharedPreference(context, WHITE_LIST)
    }

    fun setBlackList(context: Context, black: String) {
        SharedPrefUtil.writeStringToSharedPreference(context, BLACK_LIST, black)
    }

    fun getBlackList(context: Context): String? {
        return SharedPrefUtil.readStringFromSharedPreference(context, BLACK_LIST)
    }

    fun setStatus(context: Context, status: String) {
        SharedPrefUtil.writeStringToSharedPreference(context, STATUS, status)
    }

    fun getStatus(context: Context): String? {
        return SharedPrefUtil.readStringFromSharedPreference(context, STATUS)
    }

}