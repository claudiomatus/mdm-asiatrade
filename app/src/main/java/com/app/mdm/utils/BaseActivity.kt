package com.app.mdm.utils

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.app.mdm.R

abstract class BaseActivity : AppCompatActivity()  {

    lateinit var dialog: Dialog

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showProgress(activity: Activity): Dialog {
        val builder = AlertDialog.Builder(activity)
        builder.setCancelable(false)
        builder.setView(R.layout.progress_common)
        dialog = builder.create()
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
        return dialog
    }

    fun hideProgress() {
        dialog.dismiss()
    }
}