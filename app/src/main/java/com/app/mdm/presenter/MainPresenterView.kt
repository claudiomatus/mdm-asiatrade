package com.app.mdm.presenter

interface MainPresenterView {

    fun getJsonResult(`object`: Any, code: ResponseCode)

    fun showProgressUi()

    fun hideProgressUi()

}