package com.app.mdm.presenter


import com.app.mdm.model.ApiResponseModel
import com.app.mdm.model.ApplicationInfoModel
import com.app.mdm.retrofit.RetrofitManager
import com.app.mdm.utils.Constants.INSTALLED_APPS
import com.app.mdm.utils.Constants.MAC
import com.app.mdm.utils.Constants.NAME
import com.app.mdm.utils.Constants.NO_INTERNET_CONNECTION
import com.app.mdm.utils.Constants.PACKAGE
import com.app.mdm.utils.GlobalFunction
import com.app.mdm.utils.MyApplication
import com.app.mdm.utils.SharedPrefHelper
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException
import java.net.UnknownHostException



/** to call api*/

class ApiPresenter(private val mMainPresenterView: MainPresenterView)  {


    fun callInstalledApps(list: ArrayList<ApplicationInfoModel>, mac :String){

        val jsonArray = JsonArray()
        val main = JsonObject()
        for (i in list.indices){
            val jsonObject =  JsonObject()
            jsonObject.addProperty(MAC, mac)
            jsonObject.addProperty(PACKAGE,list[i].packageName?.trim())
            jsonObject.addProperty(NAME,list[i].label?.trim())
            jsonArray.add(jsonObject)
        }
        main.add(INSTALLED_APPS,jsonArray)

        RetrofitManager.getWebServices().callInstalledApps(main).enqueue(object :
            Callback<List<ApiResponseModel>>{
            override fun onResponse(call: Call<List<ApiResponseModel>>, response: Response<List<ApiResponseModel>>) {

                if(response.code() == 200){
                    val objects = response.body()
                    mMainPresenterView.getJsonResult(objects!!, ResponseCode.INSTALLED)
                }
            }

            override fun onFailure(call: Call<List<ApiResponseModel>>, t: Throwable) {


                if (t is UnknownHostException ||t is SocketTimeoutException) {
                    GlobalFunction.showToast(NO_INTERNET_CONNECTION)
                }  else {
                    GlobalFunction.showToast(t.message.toString())
                }
            }

        })
    }


    fun callBlockedApps(packageName : String){
        val jsonArray = JsonArray()
        val main = JsonObject()
        val jsonObject =  JsonObject()

        jsonObject.addProperty(MAC, SharedPrefHelper.getMacId(MyApplication.getAppInstance()))
        jsonObject.addProperty(PACKAGE,packageName)
        jsonObject.addProperty(NAME,packageName)
        jsonArray.add(jsonObject)

        main.add("blockedApps",jsonArray)

        RetrofitManager.getWebServices().callBlockedApps(main).enqueue(object  : Callback<ResponseBody>{
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if(response.code() == 200){

                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                if (t is UnknownHostException ||t is SocketTimeoutException) {
                    GlobalFunction.showToast(NO_INTERNET_CONNECTION)
                }  else {
                    GlobalFunction.showToast(t.message.toString())
                }
            }

        })


    }


    fun callNewInstalledApp(packageName : String){
        val jsonArray = JsonArray()
        val main = JsonObject()
        val jsonObject =  JsonObject()

        jsonObject.addProperty(MAC, SharedPrefHelper.getMacId(MyApplication.getAppInstance()))
        jsonObject.addProperty(PACKAGE,packageName)
        jsonObject.addProperty(NAME,packageName)
        jsonArray.add(jsonObject)

        main.add(INSTALLED_APPS,jsonArray)

        RetrofitManager.getWebServices().callNewInstalledApp(main).enqueue(object  : Callback<ResponseBody>{
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if(response.code() == 200){

                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                if (t is UnknownHostException ||t is SocketTimeoutException) {
                    GlobalFunction.showToast(NO_INTERNET_CONNECTION)
                }  else {
                    GlobalFunction.showToast(t.message.toString())
                }
            }

        })


    }
}