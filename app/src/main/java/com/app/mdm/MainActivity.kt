package com.app.mdm


import android.app.AppOpsManager
import android.app.Dialog
import android.app.admin.DevicePolicyManager
import android.content.*
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Process
import android.provider.Settings
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.app.mdm.adapter.ApplicationListAdapter
import com.app.mdm.databinding.ActivityMainBinding
import com.app.mdm.model.ApiResponseModel
import com.app.mdm.model.ApplicationInfoModel
import com.app.mdm.presenter.ApiPresenter
import com.app.mdm.presenter.MainPresenterView
import com.app.mdm.presenter.ResponseCode
import com.app.mdm.receiver.BackgroundService
import com.app.mdm.receiver.DemoDeviceAdmin
import com.app.mdm.receiver.InstallReceiver
import com.app.mdm.utils.BaseActivity
import com.app.mdm.utils.Constants
import com.app.mdm.utils.Constants.BLOCKED
import com.app.mdm.utils.Constants.BROADCAST_KEY

import com.app.mdm.utils.Constants.PACKAGE
import com.app.mdm.utils.Constants.REFRESH_KEY
import com.app.mdm.utils.SharedPrefHelper
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.reflect.TypeToken
import io.socket.client.IO
import io.socket.client.Socket
import io.socket.engineio.client.transports.WebSocket
import org.json.JSONArray
import org.json.JSONObject
import java.lang.reflect.Type
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : BaseActivity(), MainPresenterView {


    private var devicePolicyManager: DevicePolicyManager? = null
    private var demoDeviceAdmin: ComponentName? = null
    private var binding: ActivityMainBinding? = null
    private var applicationListAdapter: ApplicationListAdapter? = null
    private var socket: Socket? = null
    private var apiPresenter: ApiPresenter? = null
    var apps: ArrayList<ApplicationInfoModel> = ArrayList()
    var blackList: ArrayList<String> = ArrayList()
    private var isReceiverRegistered: Boolean = false
    private var mReceiver: BroadcastReceiver? = null

    init {
        mReceiver = object : BroadcastReceiver() {
            override fun onReceive(p0: Context?, intent: Intent?) {

                when (intent?.action) {

                    BROADCAST_KEY -> {

                        if (intent.hasExtra(Constants.WHITE_LIST_APP)) {
                            apiPresenter?.callNewInstalledApp(
                                intent.getStringExtra(Constants.WHITE_LIST_APP).toString()
                            )
                        } else if (intent.hasExtra(BLOCKED)) {
                            blackList.add(intent.getStringExtra(BLOCKED).toString())

                            apiPresenter?.callBlockedApps(intent.getStringExtra(BLOCKED).toString())

                        }

                    }
                    REFRESH_KEY ->{
                        val gson = Gson()
                        val jsonResponse: String = SharedPrefHelper.getWhiteList(p0!!).toString()
                        val type: Type = object : TypeToken<List<ApplicationInfoModel?>?>() {}.type
                        apps = gson.fromJson(jsonResponse, type)
                        applicationListAdapter?.showList(apps)

                        applicationListAdapter?.notifyDataSetChanged()
                    }


                }
            }


        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this, R.layout.activity_main
        )

        apiPresenter = ApiPresenter(this)
        applicationListAdapter = ApplicationListAdapter()

        requestUsageStatsPermission()
        devicePolicyManager = getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
        demoDeviceAdmin = ComponentName(this, DemoDeviceAdmin::class.java)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {

            val intent = Intent(
                Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:$packageName")
            )
            startActivityForResult(intent, 1)
        }


        val intent = Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN)
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, demoDeviceAdmin)
        intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "Disable app")
        startActivityForResult(intent, 1)


        if (SharedPrefHelper.getMacId(this) == null || SharedPrefHelper.getMacId(this) == "") {
            val uniqueID: String = UUID.randomUUID().toString().trim()
            getApplicationsInfo(uniqueID)

            SharedPrefHelper.setMacId(this, uniqueID)

        } else {

            apps.clear()
            val gson = Gson()
            val jsonResponse: String = SharedPrefHelper.getWhiteList(this).toString()
            val type: Type = object : TypeToken<List<ApplicationInfoModel?>?>() {}.type
            apps = gson.fromJson(jsonResponse, type)
            applicationListAdapter?.showList(apps)
            binding?.recyclerView?.adapter = applicationListAdapter
            startService(Intent(this, BackgroundService::class.java))

        }


        registerLocalReceiver()

    }

    private fun getApplicationsInfo(uniqueID: String): List<ApplicationInfoModel> {

        var apps: ArrayList<ApplicationInfoModel> = ArrayList()
        apps.clear()

        val list = packageManager.getInstalledPackages(PackageManager.GET_META_DATA)
        for (i in list.indices) {
            val packageInfo = list[i]
            if (packageInfo!!.applicationInfo.flags and ApplicationInfo.FLAG_SYSTEM == 0) {

                val info = ApplicationInfoModel()
                info.label = packageInfo.applicationInfo.loadLabel(packageManager).toString()
                info.name = packageInfo.applicationInfo.name
                info.packageName = packageInfo.applicationInfo.packageName

                apps.add(info)
            }
        }


        apiPresenter?.callInstalledApps(apps, uniqueID)



        return apps
    }

    override fun onDestroy() {
        super.onDestroy()

        try {
            unRegisterLocalReceiver()
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        }

    }

    override fun onStart() {
        super.onStart()
        connectSocket()
    }

    override fun onStop() {
        super.onStop()
        socket?.close()
    }


    /** connect socket */
    private fun connectSocket() {
        try {

            socket = IO.socket("https://socket.asiatrade.polite.cl")
            val opts = IO.Options()
            opts.transports = arrayOf(WebSocket.NAME)
            socket?.on(Socket.EVENT_CONNECT) {

                socket?.on("whitelist") {
                    val jsonArray = JSONArray(it[0].toString())
                    setSocketModel(jsonArray)

                }

                socket?.on("standard") {

                    val jsonObject = JSONObject(it[0].toString())
                    val status = jsonObject.getString("status")
                    SharedPrefHelper.setStatus(this,status)

                }

                socket?.on("block") {
                    val jsonObject = JSONObject(it[0].toString())
                    val status = jsonObject.getString("status")
                    SharedPrefHelper.setStatus(this,status)

                }

                socket?.on("unlock") {
                    val jsonObject = JSONObject(it[0].toString())
                    val status = jsonObject.getString("status")
                    SharedPrefHelper.setStatus(this,status)

                }


            }?.on(Socket.EVENT_CONNECT_ERROR) {


            }?.on(Socket.EVENT_DISCONNECT) {

            }
            socket?.connect()
        } catch (ignored: Exception) {

        }
    }

    override fun getJsonResult(`object`: Any, code: ResponseCode) {

        val response = `object` as List<ApiResponseModel>

        for (i in response.indices) {
            val apiResponseModel = ApplicationInfoModel()
            apiResponseModel.packageName = response[i]._package
            apiResponseModel.label = response[i].name
            apiResponseModel.name = response[i].name
            apps.add(apiResponseModel)

        }

        val gson = Gson()
        val jsonResponse = gson.toJson(apps)
        SharedPrefHelper.setWhiteList(this, jsonResponse)
        applicationListAdapter?.showList(apps)
        binding?.recyclerView?.adapter = applicationListAdapter

        startService(Intent(this, BackgroundService::class.java))

    }

    override fun showProgressUi() {
        showProgress(this)
    }

    override fun hideProgressUi() {
        hideProgress()
    }

    private fun requestUsageStatsPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
            && !hasUsageStatsPermission(this)
        ) {
            startActivity(Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS))
        }
    }


    fun hasUsageStatsPermission(context: Context): Boolean {
        val appOps =
            context.getSystemService(APP_OPS_SERVICE) as AppOpsManager
        val mode = appOps.checkOpNoThrow(
            "android:get_usage_stats",
            Process.myUid(), context.packageName
        )
        return mode == AppOpsManager.MODE_ALLOWED
    }

    private fun registerLocalReceiver() {
        val localBroadcastManager = LocalBroadcastManager.getInstance(this)
        val intentFilter = IntentFilter()
        intentFilter.addAction(BROADCAST_KEY)
        intentFilter.addAction(REFRESH_KEY)
        localBroadcastManager.registerReceiver(this.mReceiver!!, intentFilter)
        isReceiverRegistered = true
    }

    private fun unRegisterLocalReceiver() {
        val localBroadcastManager = LocalBroadcastManager.getInstance(this)
        if (isReceiverRegistered) {
            localBroadcastManager.unregisterReceiver(this.mReceiver!!)
            isReceiverRegistered = false
        }
    }

    private fun setSocketModel(response: JSONArray) {


        apps.clear()

        for (i in 0 until response.length()) {

            val applicationInfoModel = ApplicationInfoModel()
            applicationInfoModel.label = response.getJSONObject(i).getString("name")
            applicationInfoModel.name = response.getJSONObject(i).getString("name")
            applicationInfoModel.packageName = response.getJSONObject(i).getString("package")
            apps.add(applicationInfoModel)
        }

        val gson = Gson()
        val jsonResponse = gson.toJson(apps)
        SharedPrefHelper.setWhiteList(this, jsonResponse)


        val intent = Intent(REFRESH_KEY)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)

    }

}
