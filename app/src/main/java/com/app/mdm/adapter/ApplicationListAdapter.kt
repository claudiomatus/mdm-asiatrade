package com.app.mdm.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.app.mdm.R
import com.app.mdm.model.ApiResponseModel
import com.app.mdm.model.ApplicationInfoModel
import de.hdodenhof.circleimageview.CircleImageView

class ApplicationListAdapter :
    RecyclerView.Adapter<ApplicationListAdapter.ApplicationListViewHolder>() {

    var appsList: ArrayList<ApplicationInfoModel> = arrayListOf()


    fun showList(historyList: ArrayList<ApplicationInfoModel>) {

        appsList = historyList

    }

    class ApplicationListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        val tvPackageName: AppCompatTextView = itemView.findViewById(R.id.tvPackageName)
        val tvName: AppCompatTextView = itemView.findViewById(R.id.tvName)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ApplicationListViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_application_adapter, parent, false)

        return ApplicationListViewHolder(view)

    }

    override fun onBindViewHolder(holder: ApplicationListViewHolder, position: Int) {

        holder.tvName.text = appsList[position].label
        holder.tvPackageName.text = appsList[position].packageName

    }

    override fun getItemCount(): Int {
        return appsList.size
    }
}