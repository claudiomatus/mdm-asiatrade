package com.app.mdm.retrofit

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitManager {

    /** Retrofit to call the API"S*/

    private lateinit var webServices: WebServices
    const val API_END_POINT = "https://api.asiatrade.polite.cl/"

    fun getWebServices(): WebServices {

        val httpClientBuilder = OkHttpClient.Builder()
        httpClientBuilder.readTimeout(45, TimeUnit.SECONDS)
            .connectTimeout(45, TimeUnit.SECONDS)

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        httpClientBuilder.addInterceptor(interceptor)

        val gson = GsonBuilder().setLenient().create()
        val retrofit = Retrofit.Builder()
            .baseUrl(API_END_POINT)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(httpClientBuilder.build())
            .build()

        webServices = retrofit.create(WebServices::class.java)

        return webServices
    }
}