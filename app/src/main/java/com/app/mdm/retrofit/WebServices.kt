package com.app.mdm.retrofit

import com.app.mdm.model.ApiResponseModel
import com.google.gson.JsonObject
import org.json.JSONArray
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface WebServices {

    @POST("app/installed")
    fun callInstalledApps(
        @Body jsonObject: JsonObject
    ): Call<List<ApiResponseModel>>


    @POST("app/blocked")
    fun callBlockedApps(
        @Body jsonObject: JsonObject
    ): Call<ResponseBody>

    @POST("app/installed")
    fun callNewInstalledApp(
        @Body jsonObject: JsonObject
    ): Call<ResponseBody>



}