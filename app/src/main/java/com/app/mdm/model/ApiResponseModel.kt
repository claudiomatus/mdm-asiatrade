package com.app.mdm.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


/** Model class to fetch the response from API*/
class ApiResponseModel() : Parcelable {

    @SerializedName("id")
    @Expose
    var id: String? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("package")
    @Expose
    var _package: String? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        name = parcel.readString()
        _package = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(name)
        parcel.writeString(_package)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ApiResponseModel> {
        override fun createFromParcel(parcel: Parcel): ApiResponseModel {
            return ApiResponseModel(parcel)
        }

        override fun newArray(size: Int): Array<ApiResponseModel?> {
            return arrayOfNulls(size)
        }
    }

}