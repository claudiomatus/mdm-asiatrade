package com.app.mdm.model

import android.graphics.drawable.Drawable


class ApplicationInfoModel {
    var label: String? = null
    var name: String? = null
    var packageName: String? = null


    override fun toString(): String {
        return ("label:" + label + " name:" + name + " packageName:" + packageName)
    }
}