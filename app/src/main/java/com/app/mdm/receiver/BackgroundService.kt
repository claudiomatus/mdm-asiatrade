package com.app.mdm.receiver


import android.app.*
import android.app.usage.UsageStats
import android.app.usage.UsageStatsManager

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Build
import android.os.IBinder

import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat

import com.app.mdm.utils.Constants
import com.app.mdm.utils.SharedPrefHelper
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import java.util.*
import kotlin.collections.ArrayList
import android.app.ActivityManager
import android.app.ActivityManager.RunningAppProcessInfo
import android.os.Process
import com.app.mdm.utils.MyApplication
import android.app.Application
import android.util.Log
import com.app.mdm.utils.Constants.BLOCK
import com.app.mdm.utils.Constants.BLOCKED
import com.app.mdm.utils.Constants.UNLOCK
import java.lang.Exception


class BackgroundService() : Service() {

    var counter = 0
    private var mActivityManager: ActivityManager? = null
    var blackList: ArrayList<String> = ArrayList()

    private var broadcastReceiver: InstallReceiver = InstallReceiver()
    private var isReceiverRegistered: Boolean = false

    override fun onCreate() {
        super.onCreate()
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) createNotificationChanel() else startForeground(
            1,
            Notification()
        )
        printForegroundTask()

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChanel() {
        val NOTIFICATION_CHANNEL_ID = "com.getlocationbackground"
        val channelName = "Background Service"
        val chan = NotificationChannel(
            NOTIFICATION_CHANNEL_ID,
            channelName,
            NotificationManager.IMPORTANCE_NONE
        )
        chan.lightColor = Color.BLUE
        chan.setShowBadge(false)
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val manager =
            (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
        manager.createNotificationChannel(chan)
        val notificationBuilder =
            NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
        val notification: Notification = notificationBuilder.setOngoing(true)
            .setContentTitle("App is running count::" + counter)
            .setPriority(NotificationManager.IMPORTANCE_MIN)
            .setCategory(Notification.CATEGORY_SERVICE)
            .build()
        startForeground(2, notification)


    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        startTimer()
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        stoptimertask()
        val broadcastIntent = Intent()
        broadcastIntent.action = "restartservice"
        broadcastIntent.setClass(this, RestartBackgroundService::class.java)
        this.sendBroadcast(broadcastIntent)
    }

    private var timer: Timer? = null
    private var timerTask: TimerTask? = null
    fun startTimer() {
        timer = Timer()
        timerTask = object : TimerTask() {
            override fun run() {
               if(!isReceiverRegistered){
                   registerLocalReceiver()
               }
                printForegroundTask()


            }
        }
        timer!!.schedule(
            timerTask,
            0,
            2000
        )
    }

    fun stoptimertask() {
        if (timer != null) {
            timer!!.cancel()
            timer = null
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }


    /** to check which app is running in background*/
    private fun printForegroundTask(): String {

        if(SharedPrefHelper.getStatus(this).equals(UNLOCK)){

        }else {

            val gson = Gson()
            val jsonResponse: String = SharedPrefHelper.getBlackList(this).toString()
            val type: Type = object : TypeToken<List<String>>() {}.type
            if (type != null) {
                try {
                    blackList = gson.fromJson(jsonResponse, type)
                } catch (e: NullPointerException) {
                    e.printStackTrace()
                }

            }


            var currentApp = "NULL"

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val usm = this.getSystemService(USAGE_STATS_SERVICE) as UsageStatsManager
                val time = System.currentTimeMillis()
                val appList =
                    usm.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 1000 * 1000, time)
                if (appList != null && appList.size > 0) {
                    val mySortedMap: SortedMap<Long, UsageStats> = TreeMap()
                    for (usageStats in appList) {
                        mySortedMap[usageStats.lastTimeUsed] = usageStats
                    }
                    if (mySortedMap != null && !mySortedMap.isEmpty()) {
                        currentApp = mySortedMap[mySortedMap.lastKey()]!!.packageName

                    }

                }
                blockDialog(currentApp, this)


            } else {
                mActivityManager = this.getSystemService(ACTIVITY_SERVICE) as ActivityManager
                val tasks = mActivityManager?.runningAppProcesses
                currentApp = tasks?.get(0)?.processName.toString()
                blockDialog(currentApp, this)
            }

            return currentApp

        }

        return ""

    }

    /** i will check which app is running (black list or white list)*/
    fun blockDialog(currentApp: String, context: Context) {
        Log.d("++++", "blockDialog: "+currentApp)
        if(SharedPrefHelper.getBlackList(this).equals(BLOCKED)){
            if(currentApp == "com.android.vending"){
                showHomeScreen(context)
            }

        }else{
            /** black list is the list of installed apps which is not in whitelist*/
            if (blackList.contains(currentApp)) {
                showHomeScreen(context)
            }

        }

    }

    /** to block the app*/
    fun showHomeScreen(context: Context): Boolean {
        val startMain = Intent(Intent.ACTION_MAIN)
        startMain.addCategory(Intent.CATEGORY_HOME)
        startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(startMain)
        return true
    }

    private fun registerLocalReceiver() {

        registerReceiver(broadcastReceiver, IntentFilter().apply {
            addAction(Intent.ACTION_PACKAGE_ADDED)
            addAction(Intent.ACTION_PACKAGE_REMOVED)
            addDataScheme(Constants.PACKAGE)
        })

        isReceiverRegistered = true
    }

    private fun unRegisterLocalReceiver() {

        if (isReceiverRegistered) {
            unregisterReceiver(broadcastReceiver)
            isReceiverRegistered = false
        }
    }



}



