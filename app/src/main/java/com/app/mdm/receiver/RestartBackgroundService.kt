package com.app.mdm.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import com.app.mdm.utils.GlobalFunction
import com.app.mdm.MainActivity




class RestartBackgroundService : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {



        if (Intent.ACTION_BOOT_COMPLETED == intent?.action){
                startServiceIntent(context)

        }else{
            startServiceIntent(context)
        }
    }

    fun startServiceIntent(context: Context?){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context!!.startForegroundService(Intent(context, BackgroundService::class.java))
        } else {
            context!!.startService(Intent(context, BackgroundService::class.java))
        }
    }
}