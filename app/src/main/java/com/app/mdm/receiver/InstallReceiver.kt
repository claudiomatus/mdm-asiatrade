package com.app.mdm.receiver



import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.app.mdm.model.ApplicationInfoModel
import com.app.mdm.utils.Constants
import com.app.mdm.utils.Constants.BLOCKED
import com.app.mdm.utils.Constants.BROADCAST_KEY
import com.app.mdm.utils.Constants.WHITE_LIST_APP
import com.app.mdm.utils.SharedPrefHelper
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import java.util.*


class InstallReceiver : BroadcastReceiver() {


    var whiteList: ArrayList<ApplicationInfoModel> = ArrayList()
    var packgeList: ArrayList<String> = ArrayList()
    var blackList: ArrayList<String> = ArrayList()

    override fun onReceive(context: Context?, intent: Intent?) {



        /** to check when new app is installed*/
       if( SharedPrefHelper.getStatus(context!!).equals(Constants.UNLOCK)){

       }else if (intent?.action.equals(Intent.ACTION_PACKAGE_ADDED)) {
            val packageName = intent?.data?.schemeSpecificPart
            whiteList.clear()
            packgeList.clear()
            val gson = Gson()
            val jsonResponse: String = SharedPrefHelper.getWhiteList(context).toString()
            val type: Type = object : TypeToken<List<ApplicationInfoModel?>?>() {}.type
            whiteList = gson.fromJson(jsonResponse, type)

            for (i in whiteList.indices) {
                packgeList.add(whiteList[i].packageName.toString())
            }
            val intent = Intent(BROADCAST_KEY)
            if (packgeList.contains(packageName)) {
                intent.putExtra(WHITE_LIST_APP, packageName)
            } else {
                intent.putExtra(BLOCKED, packageName)
                blackList.add(packageName.toString())
                val gson = Gson()
                val jsonResponse = gson.toJson(blackList)
                SharedPrefHelper.setBlackList(context, jsonResponse)
            }
            LocalBroadcastManager.getInstance(context).sendBroadcast(intent)

        }
    }


}